# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of ku.po to Kurdish
# Kurdish messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Rizoyê Xerzî <riza dot seckin at gmail dot com>
# Erdal Ronahî <erdal.ronahi@gmail.com>, 2008, 2010.
# Lecwan Munzur <lecwan@riseup.net>, 2020.
#
# Translations from iso-codes:
#   Erdal Ronahi <erdal.ronahi@gmail.com>, 2005.
# Erdal Ronahi <erdal dot ronahi at gmail dot com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: ku\n"
"Report-Msgid-Bugs-To: partman-auto@packages.debian.org\n"
"POT-Creation-Date: 2024-11-26 20:02+0000\n"
"PO-Revision-Date: 2021-05-21 05:32+0000\n"
"Last-Translator: Jacque Fresco <aidter@use.startmail.com>\n"
"Language-Team: Kurdish Team http://pckurd.net\n"
"Language: ku\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid "Help on partitioning"
msgstr "Alîkariya partîsiyonkirinê"

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"Partitioning a hard drive consists of dividing it to create the space needed "
"to install your new system.  You need to choose which partition(s) will be "
"used for the installation."
msgstr ""
"Partîsiyonkirin an dabeşkirina dîskeke sabît parvekirina wê ye. Bi wî rengî "
"cihê vala ji bo pergala te ya nû tê afirandin.  Divê tu partîsiyona an "
"partîsiyonan ji bo sazkirinê hilbijêrî."

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid "Select a free space to create partitions in it."
msgstr "Qadeke vala ya tu yê li ser wê beşan pêk bîne hilbijêrî."

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"Select a device to remove all partitions in it and create a new empty "
"partition table."
msgstr ""
"Ji bo ku hemû beş biavêjî navî, cîhazekê hilbijêre û beşeke vala ya nû "
"biafirîne."

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"Select a partition to remove it or to specify how it should be used. At a "
"bare minimum, you need one partition to contain the root of the file system "
"(whose mount point is /).  Most people also feel that a separate swap "
"partition is a necessity.  \"Swap\" is scratch space for an operating "
"system, which allows the system to use disk storage as \"virtual memory\"."
msgstr ""
"Ji bo avêtinê partîsiyonê hilbijêre an jî bi awayekî vekirî diyar bike bê "
"çawa bê bikaranîn. Ji te re partîsiyoneke ku pergala pelan a rootê bihewîne "
"divê (beşa xala mountê /).  Gelek kes veqetandina partîsiyoneke swap pêwist "
"dibîne.  \"Swap\" ji bo pergalên xebatê cihekî xêzî ne ku dihêlin pergal, "
"depokirina dîskê wekî \"virtual memory\" bi kar bînin."

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"When the partition is already formatted you may choose to keep and use the "
"existing data in the partition.  Partitions that will be used in this way "
"are marked with \"${KEEP}\" in the main partitioning menu."
msgstr ""
"Dema ku beş jixwe formatbûyî be, dikarî daneyên heyî di beşê de biparêzî û "
"bi kar bînî.  Beşên wisa dê di pêşeka beşan de bi \"${KEEP}\" bên nîşankirin."

#. Type: note
#. Description
#. :sl1:
#: ../partman-auto.templates:1001
msgid ""
"In general you will want to format the partition with a newly created file "
"system.  NOTE: all data in the partition will be irreversibly deleted.  If "
"you decide to format a partition that is already formatted, it will be "
"marked with \"${DESTROY}\" in the main partitioning menu.  Otherwise it will "
"be marked with \"${FORMAT}\"."
msgstr ""
"Bi giştî tu yê bixwazî ku beşê bi pergaleke nû ya pelan format bikî.  NÎŞE: "
"hemû daneyên di partîsiyonê de dê jê bên birin.  Heke biryar bidî ku "
"partîsiyon jixwe formatbûyî ye, ew ê di pêşeka beşan de bi \"${DESTROY}\" bê "
"nîşankirin.  Wekî din dê bi \"${FORMAT}\" bê nîşankirin."

#. Type: text
#. Description
#. :sl1:
#: ../partman-auto.templates:2001
msgid "Please wait..."
msgstr "Ji kerema xwe re bisekine..."

#. Type: text
#. Description
#. :sl1:
#: ../partman-auto.templates:3001
msgid "Computing the new partitions..."
msgstr "Partîsiyonên nu dihesibîne..."

#. Type: error
#. Description
#. :sl2:
#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:4001 ../partman-auto.templates:5001
msgid "Failed to partition the selected disk"
msgstr "Partîsiyonkirina diska hilbijartî serneket"

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:4001
msgid ""
"This probably happened because the selected disk or free space is too small "
"to be automatically partitioned."
msgstr ""
"Bi îhtimalê ev derket holê ji ber ku disk an cihê vala yê hilbijartê ji bo "
"partîsiyonkirina bixweser gelek biçûk e."

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:5001
msgid ""
"This probably happened because there are too many (primary) partitions in "
"the partition table."
msgstr ""
"Bi îhtimalê ev derket holê ji ber ku hejmara partîsiyonan (yên primary) di "
"tabloya partîsiyonan zêde ye."

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:6001 ../partman-auto.templates:10001
msgid "Partitioning method:"
msgstr "Awayê partîsiyonkirinê:"

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:6001
msgid ""
"The installer can guide you through partitioning a disk (using different "
"standard schemes) or, if you prefer, you can do it manually. With guided "
"partitioning you will still have a chance later to review and customise the "
"results."
msgstr ""
"Bernameya sazkirinê ji bo dabeşkirina dîskê (li gorî şêmayên standard) wê ji "
"aliyê dikarî bibe alîkar. Heke tu bixwazî tu dikarî vî karî bi destan jî "
"bike. Heke tu di dabeşkirinê de amûra ku wê ji te re rêberiyê bike hilbijêrî "
"wê fersendekê bide te da ku tu encamên wê bibîne û ji bo ku tu karibî taybet "
"bikî."

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:6001 ../partman-auto.templates:10001
msgid ""
"If you choose guided partitioning for an entire disk, you will next be asked "
"which disk should be used."
msgstr ""
"Heke ji bo ketanekê dabeşkirineke ji rêbernamê hilbijêrî, dê di gava pêş de "
"ji te bê pirsîn ka bila kîjan dîsk bê bikaranîn."

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:11001
msgid "Partitioning scheme:"
msgstr "Plana partîsiyonkirinê:"

#. Type: select
#. Description
#. :sl1:
#. "Selected for partitioning" can be either an entire disk
#. of "the largest continuous free space" on an existing disk
#. TRANSLATORS, please take care to choose something appropriate for both
#.
#. It is followed by a variable giving the chosen disk, hence the colon
#. at the end of the sentence. Please keep it.
#: ../partman-auto.templates:11001
msgid "Selected for partitioning:"
msgstr "Ji bo partîsiyonkirinê hilbijêre:"

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:11001
msgid ""
"The disk can be partitioned using one of several different schemes. If you "
"are unsure, choose the first one."
msgstr ""
"Ji bo partîsiyonkirina vê diskê çend planên cuda hene. Heke tu baş nezanî, "
"ya yekemîn hilbijêre."

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:12001
msgid "Unusable free space"
msgstr "Cihê vala nayê bikaranîn"

#. Type: error
#. Description
#. :sl2:
#: ../partman-auto.templates:12001
msgid ""
"Partitioning failed because the chosen free space may not be used. There are "
"probably too many (primary) partitions in the partition table."
msgstr ""
"Qada ku tu hilbijartiye ji ber ku ne di rewşeke bikêrhatî ye karê "
"dabeşkirinê bi ser neket.Wer dixuye ku di tabloya dabeşkirinê de gelek beşên "
"yekemîn heye."

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:15001
msgid "Guided partitioning"
msgstr "Partîsiyonkirina bi alîkariyê"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:16001
msgid "Guided - use the largest continuous free space"
msgstr "Bi alîkarî - cihê vala yê hevgirtî yê herî mezin bi kar bîne"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:17001
msgid "Guided - use entire disk"
msgstr "Bi alîkarî - hemû diskê bi kar bîne"

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:18001
msgid "Select disk to partition:"
msgstr "Dîska were partîsiyonkirin hilbijêre:"

#. Type: select
#. Description
#. :sl1:
#: ../partman-auto.templates:18001
msgid ""
"Note that all data on the disk you select will be erased, but not before you "
"have confirmed that you really want to make the changes."
msgstr ""
"Nîşe: Hemû daneyên li ser dîska te hilbijartiye dê bên jêbirin, lê heke "
"guhartinan çênekî dê tu tişt jê neyê birin."

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#. This is a method for partitioning - as in guided (automatic) versus manual
#: ../partman-auto.templates:19001
msgid "Manual"
msgstr "Rêber"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:20001
msgid "Automatically partition the free space"
msgstr "Cihê vala bixweser partîsiyon bike"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:21001
msgid "All files in one partition (recommended for new users)"
msgstr ""
"Hemû pelan di partîsiyonekê de (ji bo bikarhênerên nû tê tawsiye kirin)"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:22001
msgid "Separate /home partition"
msgstr "Beşa /home ya cuda"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:23001
msgid "Separate /home, /var, and /tmp partitions"
msgstr "Beşên cihê yên /home, /var, û /tmp"

#. Type: text
#. Description
#. :sl2:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:24001
msgid "Small-disk (< 10GB) partitioning scheme"
msgstr "Dabeşkirina dîska biçûl (>10GB)"

#. Type: text
#. Description
#. :sl1:
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:25001
msgid "Separate /var and /srv, swap < 1GB (for servers)"
msgstr ""
